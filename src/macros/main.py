from mocodo.api import mocodo
from os import getcwd, remove, path, rename
from shutil import copy


def define_env(env):
    """
    This is the hook for the functions (new form)
    """

    @env.macro
    def mocodo_png(filename: str, title: str = "Schema"):

        fname, ext = path.splitext(filename)
        fpath = getcwd() + "/src/" + path.split(fname)[0] + "/"
        fname = path.split(fname)[1]

        _ = mocodo(
            f"-i {fpath + fname + ext} --svg_to png --colors brewer+5 --fk_format={{label}} --shapes verdana --adjust_width 0.93458 --strengthen_card 1,1(R)"
        )

        try:
            remove(getcwd() + "/" + fname + ".svg")
        except FileNotFoundError:
            pass
        try:
            remove(getcwd() + "/" + fname + "_static.svg")
        except FileNotFoundError:
            pass
        try:
            remove(getcwd() + "/" + fname + "_geo.json")
        except FileNotFoundError:
            pass

        if path.exists(fpath + fname + ".png"):
            with open(fpath + fname + ".png", "rb") as f, open(
                getcwd() + "/" + fname + ".png", "rb"
            ) as f2:
                data1 = f.read()
                data2 = f2.read()
                if data1 != data2:
                    rename(getcwd() + "/" + fname + ".png", fpath + fname + ".png")
                else:
                    remove(getcwd() + "/" + fname + ".png")
        else:
            copy(getcwd() + "/" + fname + ".png", fpath + fname + ".png")

        return f"![{title}]({path.splitext(filename)[0]}.png)"
